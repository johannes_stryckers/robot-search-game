#include "ros/ros.h"
#include "shared/is_search_ended.h"
#include "geometry_msgs/Pose.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/Header.h"

#define goalx -2.0
#define goaly 0.0
#define error 0.04

nav_msgs::Odometry position;
std_msgs::Header rfid;

void positionReceived(const nav_msgs::Odometry msg)
{
  position = msg;
}

void getRFID(const std_msgs::Header msg) {
	rfid = msg;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "is_search_ended");

  ros::NodeHandle n;

  ros::Publisher pub = n.advertise<shared::is_search_ended>("is_search_ended", 10);

  ros::Subscriber sub = n.subscribe("/p3dx/base_pose_ground_truth", 10, positionReceived);

  ros::Subscriber RFIDsub = n.subscribe("/gt_nfc", 10, getRFID);

  ros::Rate loop_rate(10);

  while (ros::ok())
  {
    shared::is_search_ended msg;

    double x = position.pose.pose.position.x;
    double y = position.pose.pose.position.y;

    if((x>goalx-error && x<goalx+error && y>goaly-error && y<goaly+error) /*|| rfid.frame_id == "042c2d9a7f2280"*/){
    	msg.is_search_ended = 1;
    }else{
        msg.is_search_ended = 0;
    }

    msg.pose = position.pose.pose;
    pub.publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}


