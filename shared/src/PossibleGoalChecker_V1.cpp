#include <vector>
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <cstdlib>
#include <math.h>

#define obstacleRange 0.40//Distance in meter
#define RFID_Range 0.04

nav_msgs::OccupancyGrid gMap;
nav_msgs::Path path;
nav_msgs::Path possibleGoals;

void getMap(const nav_msgs::OccupancyGrid msg) {
	gMap = msg;
}

void getTrajectory(const nav_msgs::Path newpath) {
	path = newpath;
}

bool poseOnPath(double x, double y) {

	int i;

	for (i = 0; i < path.poses.size(); i++) {
		if (x < path.poses.at(i).pose.position.x + RFID_Range
				&& x > path.poses.at(i).pose.position.x - RFID_Range) {
			if (y < path.poses.at(i).pose.position.y + RFID_Range
					&& y > path.poses.at(i).pose.position.y - RFID_Range) {
				return true;
			}
		}
	}
	return false;
}

/*
 * Check for obstacles in a range of obstacleRange meters from the given index
 * */
bool checkSurrounding(int index, double range, nav_msgs::OccupancyGrid map) {

	int end = range / map.info.resolution; //obstacleRange(m)/resolution(cells/m) returns number of cells
	int begin = end * -1;
	int i, j = 0;
	for (i = begin; i <= end; i++) { //check for obstacles within a radius of "obstacleRange"
		for (j = begin; j <= end; j++) { //i = x waarde, j = y waarde (rijen)
			int temp = index + i + (j * map.info.width); //Omdat 1D array is dus omzetten naar 2D

			if ((int) map.data[temp] > 0) {
				return false;
			}
		}
	}
	return true;
}

int main(int argc, char **argv) {

	ros::init(argc, argv, "PossibleGoalChecker");

	ros::NodeHandle n;

	ros::Subscriber mapSubscriber = n.subscribe<nav_msgs::OccupancyGrid>("/map", 1000, getMap);
	ros::Subscriber trajectorySubscriber = n.subscribe<nav_msgs::Path>("/trajectory", 1000, getTrajectory);

	ros::Publisher goals_pub = n.advertise<nav_msgs::Path>("goals", 10);

	ros::Rate loop_rate(10);

	while (ros::ok()) {
		if (gMap.data.size() > 0) {
			possibleGoals.poses.clear();
			int i;

			for (i = 0; i < gMap.data.size(); i++) {
				int probability = (int) gMap.data[i];

				if (probability == 0) {
					if (checkSurrounding(i, obstacleRange, gMap)) {
						double x = ((i % gMap.info.width) * gMap.info.resolution)
								+ gMap.info.origin.position.x;
						double y = (floor(i / gMap.info.width)
								* gMap.info.resolution)
								+ gMap.info.origin.position.y;
						geometry_msgs::PoseStamped pose;
						pose.pose.position.x = x;// + 0.025;
						pose.pose.position.y = y;// + 0.025;
						pose.pose.orientation.w = 1.0;
						pose.header.frame_id = "map";
						pose.header.stamp = ros::Time::now();

						if (!poseOnPath(x, y) && x <= 1.0 && x >= -2.0
								&& y <= 1.0 && y >= -2.0) {
							possibleGoals.poses.push_back(pose);
						}
					}
				}
			}

			goals_pub.publish(possibleGoals);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}

