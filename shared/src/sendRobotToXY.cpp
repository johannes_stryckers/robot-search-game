#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv) {

	ros::init(argc, argv, "Send_Robot_To");
	ros::NodeHandle n;

	//tell the action client that we want to spin a thread by default
	MoveBaseClient ac("move_base", true);

	//wait for the action server to come up
	while (!ac.waitForServer(ros::Duration(5.0))) {
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	move_base_msgs::MoveBaseGoal goal;

	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = 0.0;
	goal.target_pose.pose.position.y = 0.0;
	goal.target_pose.pose.orientation.w = 1.0;
	ROS_INFO("Sending Trixy to Position x=%f y=%f", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y);
	ac.sendGoal(goal);
	ac.waitForResult();
	if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("Hooray, the Trixy moved to x=%f y=%f", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y);
	else
		ROS_INFO("Trixy failed to move to x=%f y=%f", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y);


return 0;
}
