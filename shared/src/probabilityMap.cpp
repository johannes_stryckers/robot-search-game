#include <vector>
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <cstdlib>
#include <math.h>

#define precission 100
#define RFID_Range 0.05
#define multiplier 20

nav_msgs::OccupancyGrid map, probabilityMap;
nav_msgs::Path path;
int i = 0;

void getMap(const nav_msgs::OccupancyGrid msg) {
	map = msg;
}

void getTrajectory(const nav_msgs::Path msg) {
	path = msg;
}

int main(int argc, char **argv) {

	ros::init(argc, argv, "probabilityMap");

	ros::NodeHandle n;

	ros::Subscriber mapSubscriber = n.subscribe<nav_msgs::OccupancyGrid>("/map",
			1000, getMap);

	ros::Subscriber trajectorySubscriber = n.subscribe<nav_msgs::Path>(
			"/trajectory", 1000, getTrajectory);

	ros::Publisher pub = n.advertise<nav_msgs::OccupancyGrid>("probabilityMap",
			10);

	ros::Rate loop_rate(10);

	do {
		ros::spinOnce();
	} while (map.data.size() == 0 || path.poses.size() == 0);

	while (ros::ok()) {

		probabilityMap.info = map.info;
		probabilityMap.info.map_load_time = ros::Time::now();
		if (probabilityMap.data.size() != map.data.size()) {
			probabilityMap.data.resize(map.info.width * map.info.height);
		}

		int j = 0;

		while (i < path.poses.size()) {
			geometry_msgs::PoseStamped pose = path.poses[i++];

			for (j = 0; j < map.data.size(); j++) {
				if (map.data[j] == 0) {
					double x = ((j % map.info.width) * map.info.resolution)
							+ map.info.origin.position.x;
					double y =
							(floor(j / map.info.height) * map.info.resolution)
									+ map.info.origin.position.y;

					double distance = sqrt(
							pow(x - pose.pose.position.x, 2)
									+ pow(y - pose.pose.position.y, 2));
					probabilityMap.data[j] =
							probabilityMap.data[j] < 0 ?
									0 : probabilityMap.data[j];
					int probability = probabilityMap.data[j]
							+ (precission
									- round(
											(distance > RFID_Range ?
													RFID_Range : distance)
													* precission) * multiplier);

					probabilityMap.data[j] =
							probability > precission ? precission : probability; //Add something according to the distance
				} else {
					probabilityMap.data[j] = map.data[j]; //Add 100
				}
			}
		}

		ROS_INFO("Size of map: %d", probabilityMap.data.size());

		pub.publish(probabilityMap);
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
