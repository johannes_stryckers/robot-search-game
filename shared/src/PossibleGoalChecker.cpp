#include <vector>
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <cstdlib>
#include <math.h>

#define obstacleRange 0.50
#define RFID_Range 0.04

nav_msgs::OccupancyGrid map;
nav_msgs::Path path;
nav_msgs::Path possibleGoals;

void getMap(const nav_msgs::OccupancyGrid msg) {
	map = msg;
}

void getTrajectory(const nav_msgs::Path newpath) {
	path = newpath;
}

bool poseOnPath(double x, double y) {

	int i;

	for (i = 0; i < path.poses.size(); i++)
	{
		if (x < path.poses.at(i).pose.position.x + RFID_Range && x > path.poses.at(i).pose.position.x - RFID_Range)
		{
			if (y < path.poses.at(i).pose.position.y + RFID_Range && y > path.poses.at(i).pose.position.y - RFID_Range)
			{
				return true;
			}
		}
	}
	return false;
}

/*
 * Check for obstacles in a range of obstacleRange meters from the given index
 * */
bool checkSurrounding(int index) {

	int end = obstacleRange / map.info.resolution;
	int begin = end * -1;
	int i, j;

	for (i = begin; i <= end; i++)
	{
		for (j = begin; j <= end; j++)
		{	//j = x waarde, i = y waarde (rijen)
			int temp = index + i + (j * map.info.width); //Omdat 1D array is dus omzetten naar 2D
			if ((int) map.data[temp] > 0)
			{
				return false;
			}
		}
	}
	return true;
}

int main(int argc, char **argv) {

	ros::init(argc, argv, "PossibleGoalChecker");

	ros::NodeHandle n;

	ros::Subscriber mapSubscriber = n.subscribe<nav_msgs::OccupancyGrid>("/map", 1000, getMap);

	ros::Subscriber trajectorySubscriber = n.subscribe<nav_msgs::Path>("/trajectory", 1000, getTrajectory);

	ros::Publisher goals_pub = n.advertise<nav_msgs::Path>("goals", 10);

	ros::Rate loop_rate(10);

	while (ros::ok())
	{
		if (map.data.size() > 0)
		{
			possibleGoals.poses.clear();
			int i;

			for (i = 0; i<map.data.size(); i++)
			{
				if ((int) map.data[i] == 0)
				{
					if (checkSurrounding(i))
					{
						double x = map.info.origin.position.x + (i % map.info.width) * map.info.resolution;
						double y = map.info.origin.position.y + static_cast<int>(i / map.info.width) * map.info.resolution;
						geometry_msgs::PoseStamped pose;
						pose.pose.position.x = x;
						pose.pose.position.y = y;
						pose.pose.orientation.w = 1.0;
						pose.header.frame_id = "map";
						pose.header.stamp = ros::Time::now();
						possibleGoals.poses.push_back(pose);
					}
				}
			}
			goals_pub.publish(possibleGoals);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
