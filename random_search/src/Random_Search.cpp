#include <string>
#include <stdlib.h>
#include <ros/ros.h>
#include <ros/console.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_listener.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include "std_msgs/String.h"
#include "math.h"
#include <shared/is_search_ended.h>
#include <tf/transform_datatypes.h>


nav_msgs::OccupancyGrid map;
move_base_msgs::MoveBaseGoal goal;
shared::is_search_ended searchEnded;
bool gameOver;
double prevX = 0.0, prevY = 0.0;
nav_msgs::Path trajectory;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

void getMap(const nav_msgs::OccupancyGrid gridMap) {
	map = gridMap;
}

void isSearchEnded(const shared::is_search_ended msg) {
	searchEnded = msg;
	if (searchEnded.is_search_ended == 1 || gameOver == true) {
		ROS_INFO("GAME OVER");
		gameOver = true;
	}
}

void getTrajectory(const nav_msgs::Path msg) {
	trajectory = msg;
}

std::pair<double, double> getRobotPosition() {

	tf::TransformListener listener;
	tf::StampedTransform transform;
	listener.waitForTransform("/map", "/base_link", ros::Time::now(),
			ros::Duration(1.0));

	try {
		listener.lookupTransform("/map", "/base_link", ros::Time(0), transform);
	} catch (tf::TransformException ex) {
		ROS_ERROR("%s", ex.what());
	}

	ROS_INFO(
			"Robot position: x = %f, y = %f", transform.getOrigin().x(), transform.getOrigin().y());
	std::pair<double, double> position;
	position = std::make_pair(transform.getOrigin().x(),
			transform.getOrigin().y());
	return position;

}

void getGoal(const nav_msgs::Path path) {
	int index = rand() % path.poses.size();
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = path.poses.at(index).pose.position.x;
	goal.target_pose.pose.position.y = path.poses.at(index).pose.position.y;
	double x = goal.target_pose.pose.position.x - prevX;
	double y = goal.target_pose.pose.position.y - prevY;
	double radians = atan2(y, x);
	tf::Quaternion quaternion = tf::createQuaternionFromYaw(radians);
	geometry_msgs::Quaternion qMsg;
	tf::quaternionTFToMsg(quaternion, qMsg);
	goal.target_pose.pose.orientation = qMsg;
}

double getTravelledDistance() {
	double path_length = 0.0;
	std::vector<geometry_msgs::PoseStamped>::iterator it =
			trajectory.poses.begin();
	geometry_msgs::PoseStamped last_pose;
	last_pose = *it;
	it++;
	for (; it != trajectory.poses.end(); ++it) {
		path_length += hypot((*it).pose.position.x - last_pose.pose.position.x,
				(*it).pose.position.y - last_pose.pose.position.y);
		last_pose = *it;
	}
	return path_length;
}

int main(int argc, char** argv) {

	ros::init(argc, argv, "MapBasedGoalNode");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe<nav_msgs::OccupancyGrid>("/map", 1000,
			getMap);
	ros::Subscriber goalsub = n.subscribe<nav_msgs::Path>("/goals", 10,
			getGoal);
	ros::Subscriber subSearchEnded = n.subscribe("is_search_ended", 10,
			isSearchEnded);
	ros::Subscriber trajectorySubscriber = n.subscribe<nav_msgs::Path>(
			"/trajectory", 1000, getTrajectory);

	tf::TransformListener listener(n, ros::Duration(10));

	//tell the action client that we want to spin a thread by default
	MoveBaseClient ac("move_base", true);
	ros::Time startTime;
	do {
		startTime = ros::Time::now();
	} while (startTime.toSec() == 0);

	//wait for the action server to come up
	while (!ac.waitForServer(ros::Duration(5.0))) {
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	ros::spinOnce();

	do {
		ros::spinOnce();

		if (goal.target_pose.pose.position.x != 0 && goal.target_pose.pose.position.y != 0) {

			ROS_INFO("Sending Trixy to Position x=%f y=%f", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y);

			ac.sendGoal(goal);

			prevY = goal.target_pose.pose.position.y;
			prevX = goal.target_pose.pose.position.x;

			do {

				ros::spinOnce();

				if (searchEnded.is_search_ended == 1) {
					ac.cancelAllGoals();
					ROS_INFO("Target reached!!");
					break;
				}

			} while (ac.getState() != actionlib::SimpleClientGoalState::SUCCEEDED);

		}
	} while (searchEnded.is_search_ended == 0);
	ros::Duration timeLapse = ros::Time::now() - startTime;
	std::pair<double, double> position = getRobotPosition();
	ROS_INFO(
			"We found the target!! Position of target: x:%f, y:%f\n\rPosition of actual target: x:%f, y:%f", position.first, position.second, searchEnded.pose.position.x, searchEnded.pose.position.y);
	ROS_INFO("Passed time: %f\n\r", (double)timeLapse.toSec());
	ROS_INFO("Travelled distance: %f", getTravelledDistance());
	return 0;
}
