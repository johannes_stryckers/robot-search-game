#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Pose.h>
#include <shared/is_search_ended.h>
#include <nav_msgs/Path.h>
#include <math.h>
#include <utility>

#define error 0.04
#define turnAngle 90
#define expandRate 1

shared::is_search_ended searchEnded;
nav_msgs::Path trajectory;
int size;
int prevSize;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

std::pair <double, double> getRobotPosition()
{

	tf::TransformListener listener;
	tf::StampedTransform transform;
	listener.waitForTransform("/map", "/base_link", ros::Time::now(), ros::Duration(1.0));
	
	try{
		listener.lookupTransform("/map", "/base_link",
		ros::Time(0), transform);
	}
	catch (tf::TransformException ex){
		ROS_ERROR("%s",ex.what());
	}

	std::pair <double, double> position;
	position = std::make_pair (transform.getOrigin().x(), transform.getOrigin().y());
	return position;

}
/*
std::pair <double, double> getGoalPosition()
{

	tf::TransformListener listener;
	tf::StampedTransform transform;
	listener.waitForTransform("/map", "/move_base/current_goal", ros::Time::now(), ros::Duration(1.0));
	
	try{
		listener.lookupTransform("/map", "/move_base/current_goal",
		ros::Time(0), transform);
	}
	catch (tf::TransformException ex){
		ROS_ERROR("%s",ex.what());
	}
	
	ROS_INFO("Goal position: x = %f, y = %f", transform.getOrigin().x(), transform.getOrigin().y());
	std::pair <double, double> position;
	position = std::make_pair (transform.getOrigin().x(), transform.getOrigin().y());
	return position;

}
*/
void isSearchEnded(const shared::is_search_ended msg)
{
	searchEnded = msg;
}

void getTrajectory(const nav_msgs::Path msg)
{
	trajectory = msg;
}

double getTravelledDistance()
{
	double path_length = 0.0;
	std::vector<geometry_msgs::PoseStamped>::iterator it = trajectory.poses.begin();
	geometry_msgs::PoseStamped last_pose;
	last_pose = *it;
	it++;
	for (; it!=trajectory.poses.end(); ++it) {
	   path_length += hypot(  (*it).pose.position.x - last_pose.pose.position.x, 
		                 (*it).pose.position.y - last_pose.pose.position.y );
	   last_pose = *it;
	}
	return path_length;
}
/*
void localPlanReceived(const nav_msgs::Path msg)
{
	localPath = msg;
	prevSize = size;
	size = localPath.poses.size();
}

double getGoalDistance()
{
	std::pair<double, double> robotPosition = getRobotPosition();
	std::pair<double, double> goalPosition = getGoalPosition();
	return sqrt(pow((robotPosition.first-goalPosition.first),2)+pow((robotPosition.second-goalPosition.second),2));
}
*/
int main(int argc, char** argv)
{
	ros::init(argc, argv, "search_algorithm");

	// Read x, y and angle parameters
	double x, y, theta;
	ros::NodeHandle n;

	x = 0.1;
	y = 0.1;
	theta = turnAngle;

	ros::Time startTime;
	do{
		startTime = ros::Time::now();
	}while(startTime.toSec() == 0);
	
	//Subscriber for search ended.
	ros::Subscriber sub = n.subscribe("is_search_ended", 10, isSearchEnded);

	//Subscribe to trajectory to get travveled distance
	ros::Subscriber trajectorySubscriber = n.subscribe<nav_msgs::Path>( "/trajectory", 10, getTrajectory);
/*
	//Subscriber to calculate distance to goal
	ros::Subscriber sub1 = n.subscribe("move_base/TrajectoryPlannerROS/local_plan", 10, localPlanReceived);
*/	
	//Sleep rate
	ros::Rate loop_rate(10);

	// create the action client
	// true causes the client to spin its own thread
	MoveBaseClient ac("move_base", true);

	// Wait 5 seconds for the action server to become available
	while(!ac.waitForServer(ros::Duration(5.0))){
    		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	int count = 0;
	
	do{
		// Send a goal to move_base
		move_base_msgs::MoveBaseGoal goal;
		goal.target_pose.header.frame_id = "base_link";		//According to base_link axis.
		goal.target_pose.header.stamp = ros::Time::now();

		//Set position of goal.
		goal.target_pose.pose.position.x = x;
		goal.target_pose.pose.position.y = y;

		// Convert the Euler angle to quaternion
		double radians = theta * (M_PI/180);
		tf::Quaternion quaternion;
		quaternion = tf::createQuaternionFromYaw(radians);

		geometry_msgs::Quaternion qMsg;
		tf::quaternionTFToMsg(quaternion, qMsg);

		goal.target_pose.pose.orientation = qMsg;

		ROS_INFO("Sending goal to: x = %f, y = %f", x, y);
		ac.sendGoal(goal);

		// Wait for the robot to advance
		do{

			ros::spinOnce();
			//loop_rate.sleep();

			if(searchEnded.is_search_ended == 1)
			{
				ac.cancelAllGoals();
				ROS_INFO("Target reached!!");
				break;
			}

		}while(ac.getState() != actionlib::SimpleClientGoalState::SUCCEEDED);
		
		if(count == expandRate){
			x += error;
			y += error;
			count = 0;		
		}else{
			count++;
		}

	}while(searchEnded.is_search_ended == 0);

	ros::Duration timeLapse = ros::Time::now() - startTime;
	
	std::pair<double, double> position = getRobotPosition();

	ROS_INFO("We found the target!! Position of target: x:%f, y:%f\n\rPosition of actual target: x:%f, y:%f\n\r", position.first, position.second, searchEnded.pose.position.x, searchEnded.pose.position.y);
	ROS_INFO("Passed time: %f\n\r", (double)timeLapse.toSec());
	ROS_INFO("Travelled distance: %f", getTravelledDistance());
	return 0;
}

