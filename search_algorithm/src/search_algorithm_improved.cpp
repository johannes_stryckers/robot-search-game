#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Pose.h>
#include <shared/is_search_ended.h>
#include <nav_msgs/Path.h>
#include <math.h>
#include <utility>

#define error 0.08
#define mapResolution 0.05
#define increaseCircleDiv 0.8
#define nextGoalDivider 2.0

shared::is_search_ended searchEnded, searchFinished;
nav_msgs::Path trajectory, possibleGoals;
bool searchC = true;	//True for clockwise, false for couterclockwise
bool validGoal = true;
std::pair<double, double> startPosition;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

std::pair <double, double> getRobotPosition()
{
	tf::TransformListener listener;
	tf::StampedTransform transform;
	listener.waitForTransform("/map", "/base_link", ros::Time(0), ros::Duration(5.0));
	
	try{
		listener.lookupTransform("/map", "/base_link", ros::Time(0), transform);
	}
	catch (tf::TransformException ex){
		ROS_ERROR("%s",ex.what());
	}
	
	std::pair <double, double> position;
	position = std::make_pair (transform.getOrigin().x(), transform.getOrigin().y());
	return position;
}

void isSearchEnded(const shared::is_search_ended msg)
{
	searchEnded = msg;
	if(msg.is_search_ended == 1)
	{	
		searchFinished = msg;
	}
}

void getTrajectory(const nav_msgs::Path msg)
{
	trajectory = msg;
}

void getPossibleGoals(const nav_msgs::Path msg)
{
	possibleGoals = msg;
}

double getGoalDistance(double x, double y)
{
	std::pair<double, double> robotPosition = getRobotPosition();
	return sqrt(pow((robotPosition.first-x),2)+pow((robotPosition.second-y),2));
}

double getTravelledDistance()
{
	double path_length = 0.0;
	std::vector<geometry_msgs::PoseStamped>::iterator it = trajectory.poses.begin();
	geometry_msgs::PoseStamped last_pose;
	last_pose = *it;
	it++;
	for (; it!=trajectory.poses.end(); ++it) {
	   path_length += hypot(  (*it).pose.position.x - last_pose.pose.position.x, 
		                 (*it).pose.position.y - last_pose.pose.position.y );
	   last_pose = *it;
	}
	return path_length;
}

//Get the next x and y coordinate. Always return the coordinate even if it is not valid (to calculate the next point on the circle)
//Set a bool value true if the goal was valid. mapResolution is the resolution set in gmapping config
std::pair <double, double> getNextXY(double radius, double startAngle, double driveAngle)
{
	double x;
	double y;
	int i;
	std::pair <double, double> position;
	
	if(searchC)
	{
		x = (radius*cos(startAngle-driveAngle)) + startPosition.first;
		y = (radius*sin(startAngle-driveAngle)) + startPosition.second;
	}
	else
	{
		x = (radius*cos(startAngle+driveAngle)) + startPosition.first;
		y = (radius*sin(startAngle+driveAngle)) + startPosition.second;
	}

	for(i=0; i<possibleGoals.poses.size(); i++)
	{
		double goalx = possibleGoals.poses[i].pose.position.x;
		double goaly = possibleGoals.poses[i].pose.position.y;
		if(x > goalx - mapResolution/2 && x < goalx + mapResolution/2 && y > goaly - mapResolution/2 && y < goaly + mapResolution/2)
		{	//Valid goal
			validGoal = true;
			break;
		}
		else
		{
			validGoal = false;
		}
	}
	
	position = std::make_pair(x, y);
	
	return position;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "search_algorithm");

	ros::NodeHandle n;
	
	//Subscriber for search ended.
	ros::Subscriber sub = n.subscribe("is_search_ended", 10, isSearchEnded);

	//Subscribe to trajectory to get traveled distance
	ros::Subscriber trajectorySubscriber = n.subscribe<nav_msgs::Path>( "/trajectory", 10, getTrajectory);

	//Subscriber for possible goal locations
	ros::Subscriber goalsub = n.subscribe<nav_msgs::Path>("/goals", 10, getPossibleGoals);
	
	//Sleep rate
	ros::Rate loop_rate(10);

	// create the action client
	// true causes the client to spin its own thread
	MoveBaseClient ac("move_base", true);

	// Wait 5 seconds for the action server to become available
	while(!ac.waitForServer(ros::Duration(5.0))){
    		ROS_INFO("Waiting for the move_base action server to come up");
  	}

	//Wait untill possible goals are filled,  not nessessary because first circles work without checker
	/*do{
		ros::spinOnce();
	}while(possibleGoals.poses.size() == 0);*/
	
	double x = 0.0;		//The x coordinate for the goal
	double y = 0.0;		//The y coordinate for the goal
	double prevX = 0.0;	//Previous x coordinate
	double prevY = 0.0;	//Previous y coordinate
	double radius = 0.0;	//The radius of the circle to drive
	double circleDiv;	//The number distances the circle will be divided in
	int counter = 0;	//A counter for the number of circles driven
	bool searchFailed = false;
	startPosition = getRobotPosition(); //Get the robot start position
	searchFinished.is_search_ended = 0; //Only becomes 1 if search is ended.
	
    		ROS_INFO("Startposition = x:%f, y:%f", startPosition.first, startPosition.second);
	//Register the start time
	ros::Time startTime;
	do{
		startTime = ros::Time::now();
	}while(startTime.toSec() == 0);

	//Loop as long as the goal is not found
	do{
		double startAngle = 0.5*M_PI;//The angle at which the robot starts driving circles
		double driveDistance;	//The distance the robot has to drive along the circle
		double driveAngle;	//The angle the robot has to driva along the circle
		radius += error;	//The radius will increase with the error rate of the rfid tag
		double omtrek = 2*M_PI*radius;
		
		//As long as the circles are too small, do not divide them into less then 4 pieces
		if(counter>=4){
			circleDiv += increaseCircleDiv;
		}else{
			circleDiv = 4;
		}

		//Calculate drive distance and drive angle
		driveDistance = omtrek/circleDiv;
		driveAngle = driveDistance/radius;
		
		ROS_INFO("-----------------Starting circle %d with radius %.2f-----------------", counter, radius);
		ROS_INFO("Sending %.2f goals with distance: %.2f, and angle: %.2f", floor(circleDiv), driveDistance, driveAngle);

		int goalsPosted = 0;	//Number of goals posted on the circle
		int goalsSkipped = 0;	//Number of goals skipped on the circle

		//Loop as long as the circle is not finished
		do{			
			// Send a goal to move_base
			move_base_msgs::MoveBaseGoal goal;
			goal.target_pose.header.frame_id = "map";		//According to map axis.
			goal.target_pose.header.stamp = ros::Time::now();
			std::pair<double, double> nextGoal;
		
			//Calculate next x and y coordinates
			do{
				nextGoal = getNextXY(radius, startAngle, driveAngle);
				if(validGoal || counter < 4)
				{	//Goal is valid for processing or first 4 goals (never valid at start), break the loop
					ROS_INFO("Valid goal! x: %.2f, y: %.2f, startAngle: %.2f", nextGoal.first, nextGoal.second, startAngle);
					//calculate a new startangle
					startAngle  = atan2(nextGoal.second - startPosition.second, nextGoal.first - startPosition.first);
					break;
				}
				//Goal was not valid for processing
				ROS_INFO("Goal was not valid. startAngle: %.2f, x:%.2f, y:%.2f", startAngle, nextGoal.first, nextGoal.second);
				//calculate a new startangle
				startAngle = atan2(nextGoal.second - startPosition.second, nextGoal.first - startPosition.first);
				goalsSkipped++;

			}while(goalsSkipped+goalsPosted < floor(circleDiv));
			
			if(goalsSkipped+goalsPosted < floor(circleDiv))
			{
				//Set position of goal.
				goal.target_pose.pose.position.x = nextGoal.first;
				goal.target_pose.pose.position.y = nextGoal.second;

				// Convert the angle to quaternion
				double diffX = nextGoal.first - x;
				double diffY = nextGoal.second - y;
				double radians = atan2(diffY, diffX);

				tf::Quaternion quaternion;
				quaternion = tf::createQuaternionFromYaw(radians);

				geometry_msgs::Quaternion qMsg;
				tf::quaternionTFToMsg(quaternion, qMsg);

				goal.target_pose.pose.orientation = qMsg;
			
				//Send the goal
				ac.sendGoal(goal);

				//Store the x and y value
				x = nextGoal.first;
				y = nextGoal.second;

				double goalDistance;
			
				goalsPosted++;

				ROS_INFO("Goals skipped: %d, goals posted: %d, total goals: %.2f", goalsSkipped, goalsPosted, floor(circleDiv));

				// Wait for the robot to advance untill driveDistance/2 close to the goal or untill the goal is reached according to the action server
				do{
					//Fill the subs
					ros::spinOnce();

					if(searchFinished.is_search_ended == 1)
					{	//Goal is found! break the loop
						ac.cancelAllGoals();
						ROS_INFO("Target reached!!");
						break;
					}
			
					goalDistance = getGoalDistance(nextGoal.first, nextGoal.second);
			
					//ROS_INFO("Distance to goal: %.2f", goalDistance);

				}while(goalDistance > (driveDistance/nextGoalDivider) && ac.getState() != actionlib::SimpleClientGoalState::SUCCEEDED);
			}

		//As long as the circle is not fully searched and the object is not found
		}while(searchFinished.is_search_ended == 0 && goalsPosted+goalsSkipped < floor(circleDiv));
		//If all the goals on the circle are skipped, the total room is searched. Break the loop en output that no object was found.
		if(goalsSkipped == floor(circleDiv))
		{
			searchFailed = true;
			break;
		}
		
		counter++;

	}while(searchFinished.is_search_ended == 0);
	
	ros::Duration timeLapse = ros::Time::now() - startTime;
	
	ROS_INFO("Search ended... Start position of search: x:%f, y:%f", startPosition.first, startPosition.second);
	ROS_INFO("Parameters: Spacing between circles:%f, Resolution of map:%f, Increase parts of circle by:%f, Post new goal is dist = DriveDist / %f", error, mapResolution, increaseCircleDiv, nextGoalDivider);
	
	startPosition = getRobotPosition();
	
	if(searchFailed)
	{
		ROS_INFO("The room was searched but the target was not found...");
	}
	else
	{
		ROS_INFO("Position of target: x:%f, y:%f\nPosition of actual target: x:%f, y:%f", startPosition.first, startPosition.second, searchFinished.pose.position.x, searchFinished.pose.position.y);
	}
	ROS_INFO("Passed time: %f", (double)timeLapse.toSec());
	ROS_INFO("Travelled distance: %f", getTravelledDistance());

	return 0;
}

